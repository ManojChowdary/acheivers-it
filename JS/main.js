/*SCROLL TOP BTN*/

$(document).ready(function () {
    $(window).scroll(function () {
        if ($(this).scrollTop() > 100) {
            $('#scroll').fadeIn();
        } else {
            $('#scroll').fadeOut();
        }
    });
    $('#scroll').click(function () {
        $("html, body").animate({
            scrollTop: 0
        }, 600);
        return false;
    });
});


/*-----------------------------------------------------------------------*/
/*COURSE PAGE STICKY*/
$('body').scrollspy({
    target: "#course-sticky"
});

/*----------------------------------------------------------------------------*/

/*CAROUSEL*/
$('.carousel').carousel({
    interval: 4000
});

/*FORM VALIDATIONS*/
(function () {
    'use strict';
    window.addEventListener('load', function () {
        // Fetch all the forms we want to apply custom Bootstrap validation styles to
        var forms = document.getElementsByClassName('needs-validation');
        // Loop over them and prevent submission
        var validation = Array.prototype.filter.call(forms, function (form) {
            form.addEventListener('submit', function (event) {
                if (form.checkValidity() === false) {
                    event.preventDefault();
                    event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
        });
    }, false);
})();

/*------------------------------------*/
window.FontAwesomeConfig = {
    searchPseudoElements: true
}
/*-----------------------------------*/

/*-----------------------------------*/
$(document).ready(function () {
    // SHOWING AND HIDING MENU
    $('#button-menu').click(function () {
        if ($('#button-menu').attr('class') == 'btn-open') {

            $('.navegacion').css({
                'width': '100%',
                'background': 'rgba(0,0,0,.5)'
            }); // We show the transparent background
            $('#button-menu').removeClass('btn-open').addClass('f00d'); // We add the X icon
            $('.navegacion .menu').css({
                'left': '0px'
            }); //We show the menu

        } else {

            $('.navegacion').css({
                'width': '0%',
                'background': 'rgba(0,0,0,.0)'
            }); // We hide the transparent background
            $('#button-menu').removeClass('f00d').addClass('btn-open'); // We add the Menu icon
            $('.navegacion').css({
                'display': 'block'
            });
            $('.navegacion .submenu').css({
                'left': '-320px'
            }); // We hide the submenus
            $('.navegacion .menu').css({
                'left': '-320px'
            }); // We hide the Menu

        }
    });

    // SHOWING SUBMENU
    $('.navegacion .menu > .item-submenu a').click(function () {

        var positionMenu = $(this).parent().attr('menu'); // We look for the value of the menu attribute and save it in a variable
        console.log(positionMenu);

        $('.item-submenu[menu=' + positionMenu + '] .submenu').css({
            'left': '0px'
        }); // We show the corresponding submenu

    });

    //HIDING SUBMENU
    $('.navegacion .submenu li.go-back').click(function () {

        $(this).parent().css({
            'left': '-320px'
        }); // We hide the submenu

    });
});




/*--------------------------------------------------------------------------*/
var nav_courses = document.getElementById('nav_courses');
//When the user clicks anywhere outside of the modal, close it
nav_courses.onclick = function (event) {
    if (event.target == nav_courses) {
        nav_courses.style.width = "0%";
        document.querySelector('#nav_courses .menu').style.left = '-320px';
       $('#button-menu').removeClass('f00d').addClass('btn-open');
    }
}
